
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rahul S Agasthya
 * Stony Brook ID: 109961109
 * 
 * CSE 390 Natural Languages Processing
 * Assignment 1
 */
public class FileController {
    public String trainingCorpus;
    public String testData;
    
    public Text trainingFromFile;
    public Text testFromFile;
    
    public static String BIGRAM_WORD_1     = "Word1";
    public static String BIGRAM_WORD_2     = "Word2";
    public static String MLE               = "MLE";
    public static String LAPLACE_SMOOTHING = "LaplaceSmoothing";
    public static String ABSOLUTE_DISCOUNT = "AbsoluteDiscounting";
    public static String KATZ_BACKOFF      = "KatzBackoff";
    public static String JSON_EXT          = ".json";
    public static String FILE_PATH         = "./LanguageFiles/";

    public FileController() {}

    public FileController(String trainingCorpus, String testData) {
        this.trainingCorpus = trainingCorpus;
        this.testData = testData;
    }
    
    public String getTrainingCorpus() {
        return trainingCorpus;
    }

    public String getTestData() {
        return testData;
    }

    public void setTrainingCorpus(String trainingCorpus) {
        this.trainingCorpus = trainingCorpus;
    }

    public void setTestData(String testData) {
        this.testData = testData;
    }
    
    public void getData() throws FileNotFoundException {
        String trainingInput = new Scanner(new File(trainingCorpus)).useDelimiter("\\Z").next().trim();
        String testInput = new Scanner(new File(testData)).useDelimiter("\\Z").next().trim();
        
        trainingInput = trainingInput.trim().replaceAll("\n+", " ");
        testInput = testInput.trim().replaceAll("\n+", " ");
        trainingInput = trainingInput.trim().replaceAll("\\s+", " ");
        testInput = testInput.trim().replaceAll("\\s+", " ");
        
        trainingFromFile = new Text(trainingInput);
        testFromFile = new Text(testInput);
    }
    
    public ArrayList<String> getTrainingWords() throws FileNotFoundException {
        getData();
        ArrayList<String> sentences = trainingFromFile.getSentences();
        String inputString = "";
        for(int i=0; i<sentences.size(); i++) {
            inputString = inputString + "<s> " + sentences.get(i) + " </s> ";
        }
        trainingFromFile.setInput(inputString);
        return trainingFromFile.getWords();
    }
    
    public ArrayList<String> getTestWords() throws FileNotFoundException {
        getData();
        ArrayList<String> sentences = testFromFile.getSentences();
        String inputString = "";
        for(int i=0; i<sentences.size(); i++) {
            inputString = "<s> " + sentences.get(i) + " </s> ";
        }
        testFromFile.setInput(inputString);
        return testFromFile.getWords();
    }
    
    public List<String> getTrainingSentences() throws FileNotFoundException {
        getData();
        return trainingFromFile.getSentences();
    }
    
    public List<String> getTestSentences() throws FileNotFoundException {
        getData();
        return testFromFile.getSentences();
    }
    
    public static void saveJSONFile(ArrayList<bigramData> dataList) throws IOException {
        // BUILD THE FILE PATH
        String fileTitle = "LanguageModelFile";
        String jsonFilePath = fileTitle + JSON_EXT;
        System.out.println("The top 20 bigrams file is found in ./CSE 390 Assignment/LanguageModelFile.json");

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);

        // BUILD THE SLIDES ARRAY
        JsonArray bigrams = makeDataJsonArray(dataList);

        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        JsonObject courseJsonObject = Json.createObjectBuilder()
                .add("LANGUAGE DATA", bigrams)
                .build();

        // AND SAVE EVERYTHING AT ONCE
        jsonWriter.writeObject(courseJsonObject);
    }
    
    private static JsonArray makeDataJsonArray(ArrayList<bigramData> dataList) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (bigramData bigram : dataList) {
            JsonObject jso = makeBigramJsonObject(bigram);
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    private static JsonObject makeBigramJsonObject(bigramData data) {
        JsonObject jso = Json.createObjectBuilder()
                .add(BIGRAM_WORD_1, data.getData().getWord1())
                .add(BIGRAM_WORD_2, data.getData().getWord2())
                .add(MLE, data.getMle())
                .add(LAPLACE_SMOOTHING, data.getLs())
                .add(ABSOLUTE_DISCOUNT, data.getAd())
                .add(KATZ_BACKOFF, data.getKatz())
                .build();
        return jso;
    }
    
    public void TopBigrams(ArrayList<bigramData> data) throws IOException {
        Comparator comp1 = new JointProbabilityComparator();
        Collections.sort(data, comp1);
        String fileContent = "Joint Probability\n";
        fileContent = fileContent + String.format("%-50s%-50s%-50s%-50s%-50s%-50s%-50s", "BIGRAMS", "BIGRAM COUNT", "MAXIMUM LIKELYHOOD ESTIMATION", "LAPLACE SMOOTHING", "ABSOLUTE DISCOUNTING", "KATZ BACKOFF", "JOINT PROBABILITY") + "\n";
        fileContent = fileContent + String.format("%-50s%-50s%-50s%-50s%-50s%-50s%-50s", "-------", "------------", "-----------------------------", "-----------------", "--------------------", "------------", "-----------------") + "\n";
        for(int i=0; i<20; i++) {
            fileContent = fileContent + data.get(i).toString() + "\n";
        }
        
        Comparator comp2 = new MLEComparator();
        Collections.sort(data, comp2);
        fileContent = fileContent + "\nMLE\n";
        fileContent = fileContent + String.format("%-50s%-50s%-50s%-50s%-50s%-50s%-50s", "BIGRAMS", "BIGRAM COUNT", "MAXIMUM LIKELYHOOD ESTIMATION", "LAPLACE SMOOTHING", "ABSOLUTE DISCOUNTING", "KATZ BACKOFF", "JOINT PROBABILITY") + "\n";
        fileContent = fileContent + String.format("%-50s%-50s%-50s%-50s%-50s%-50s%-50s", "-------", "------------", "-----------------------------", "-----------------", "--------------------", "------------", "-----------------") + "\n";
        for(int i=0; i<20; i++) {
            fileContent = fileContent + data.get(i).toString() + "\n";
        }
        
        Comparator comp3 = new LaplaceComparator();
        Collections.sort(data, comp3);
        fileContent = fileContent + "\nLaplace Smoothing\n";
        fileContent = fileContent + String.format("%-50s%-50s%-50s%-50s%-50s%-50s%-50s", "BIGRAMS", "BIGRAM COUNT", "MAXIMUM LIKELYHOOD ESTIMATION", "LAPLACE SMOOTHING", "ABSOLUTE DISCOUNTING", "KATZ BACKOFF", "JOINT PROBABILITY") + "\n";
        fileContent = fileContent + String.format("%-50s%-50s%-50s%-50s%-50s%-50s%-50s", "-------", "------------", "-----------------------------", "-----------------", "--------------------", "------------", "-----------------") + "\n";
        for(int i=0; i<20; i++) {
            fileContent = fileContent + data.get(i).toString() + "\n";
        }
        
        Comparator comp4 = new ADComparator();
        Collections.sort(data, comp4);
        fileContent = fileContent + "\nAbsolute Discounting\n";
        fileContent = fileContent + String.format("%-50s%-50s%-50s%-50s%-50s%-50s%-50s", "BIGRAMS", "BIGRAM COUNT", "MAXIMUM LIKELYHOOD ESTIMATION", "LAPLACE SMOOTHING", "ABSOLUTE DISCOUNTING", "KATZ BACKOFF", "JOINT PROBABILITY") + "\n";
        fileContent = fileContent + String.format("%-50s%-50s%-50s%-50s%-50s%-50s%-50s", "-------", "------------", "-----------------------------", "-----------------", "--------------------", "------------", "-----------------") + "\n";
        for(int i=0; i<20; i++) {
            fileContent = fileContent + data.get(i).toString() + "\n";
        }
        
        Comparator comp5 = new KatzComparator();
        Collections.sort(data, comp5);
        fileContent = fileContent + "\nKatz Backoff Discounting\n";
        fileContent = fileContent + String.format("%-50s%-50s%-50s%-50s%-50s%-50s%-50s", "BIGRAMS", "BIGRAM COUNT", "MAXIMUM LIKELYHOOD ESTIMATION", "LAPLACE SMOOTHING", "ABSOLUTE DISCOUNTING", "KATZ BACKOFF", "JOINT PROBABILITY") + "\n";
        fileContent = fileContent + String.format("%-50s%-50s%-50s%-50s%-50s%-50s%-50s", "-------", "------------", "-----------------------------", "-----------------", "--------------------", "------------", "-----------------") + "\n";
        for(int i=0; i<20; i++) {
            fileContent = fileContent + data.get(i).toString() + "\n";
        }
        
        FileWriter fWriter = new FileWriter("TopBigrams.txt");
        BufferedWriter writer = new BufferedWriter(fWriter);
        writer.write(fileContent);
        writer.close();
        
        System.out.println("The top 20 bigrams file is found in ./CSE 390 Assignment/TopBigrams.txt");
    }
}
