import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rahul S Agasthya
 * Stony Brook ID: 109961109
 * 
 * CSE 390 Natural Languages Processing
 * Assignment 1
 */
public class Text {
    private String input;
    private List<String> sentences;
    private List<String> words;

    /**
     * No argument constructor.
     */
    public Text() {
    }

    /**
     * Constructor that takes one parameter.
     * @param input 
     * The text that needs to be split.
     */
    public Text(String input) {
        this.input = input;
    }

    /**
     * Getter methods.
     * Return the three data members of this class.
     */    
    public String getInput() {
        return input;
    }

    public List<String> getSentences() {
        return sentences;
    }

    public List<String> getWords() {
        return words;
    }
    
    /**
     * Setter methods.
     * Used to mutate the three data members of this class.
     */
    public void setInput(String input) {
        this.input = input;
    }

    public void setSentences(List<String> sentences) {
        this.sentences = sentences;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }
    
    public void splitSentences() {
        this.input.trim();
        sentences = Arrays.asList(input.split("\\."));
    }
    
    public void splitWords() {
        this.input.trim();
        words = Arrays.asList(input.split(" "));
    }
    
    public String toString() {
        String s = "The sentences in \"" + input + "\" are: \n";
        for(int i=0; i<sentences.size(); i++)
            s = s + (i+1) + ". "+sentences.get(i).trim() + "\n";
        s = s + "\nThe Words in \"" + input + "\" are: \n";
        for(int i=0; i<words.size(); i++)
            s = s + (i+1) + ". "+words.get(i).trim() + "\n";
        return s;
    }
}
