
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rahul S Agasthya Stony Brook ID: 109961109
 *
 * CSE 390 Natural Languages Processing Assignment 1
 */
public class LanguageModelBuilder {

    FileController files;
    ArrayList<String> trainWords, testWords;

    ArrayList<bigramWord> bigramWordList;
    ArrayList<bigramData> bigrams;

    public LanguageModelBuilder(String train, String test) throws FileNotFoundException {
        files = new FileController(train, test);
        trainWords = files.getTrainingWords();
        testWords = files.getTestWords();

        bigramWordList = new ArrayList<bigramWord>();
        bigrams = new ArrayList<bigramData>();
    }

    public void getBigramList() {
        for (int i = 0; i < trainWords.size() - 1; i++) {
            bigramWord pair = new bigramWord(trainWords.get(i), trainWords.get(i + 1));
            bigramWordList.add(pair);
        }
    }

    public ArrayList<bigramWord> getBigramWordList() {
        return bigramWordList;
    }

    public ArrayList<bigramData> getBigrams() {
        return bigrams;
    }

    public void setDataBigrams() {
        for (int i = 0; i < bigramWordList.size(); i++) {
            bigramWord words = bigramWordList.get(i);
            bigramData newPair = new bigramData();
            newPair.setData(words);
            newPair.setCount(countBigram(words));
            newPair.setCountWord1(countWord(words.word1));
            newPair.setMle(MLE(words));
            newPair.setLs(laplaceSmoothing(words));
            newPair.setAd(absoluteDiscounting(words));
            newPair.setKatz(KatzBackOff(words));
            newPair.setJp(JointProbability(words));
            bigrams.add(newPair);
        }
    }

    public int countBigram(bigramWord pair) {
        int count = 0;
        for (int j = 0; j < bigramWordList.size(); j++) {
            if (pair.word1.equals(bigramWordList.get(j).word1) && pair.word2.equals(bigramWordList.get(j).word2)) {
                count++;
            }
        }
        return count;
    }

    public int countWord(String word) {
        int countWord = 0;
        for (int j = 0; j < trainWords.size(); j++) {
            if (trainWords.get(j).equals(word)) {
                countWord++;
            }
        }
        return countWord;
    }

    public double MLE(bigramWord pair) {
        int bigramCount = countBigram(pair);
        int word1Count = countWord(pair.word1);
        return (double) bigramCount / word1Count;
    }

    public double laplaceSmoothing(bigramWord pair) {
        int bigramCount = countBigram(pair);
        int word1Count = countWord(pair.word1);

        return (double) (bigramCount + 1) / (word1Count + trainWords.size());
    }

    public double absoluteDiscounting(bigramWord pair) {
        int bigramCount = countBigram(pair);
        int word1Count = countWord(pair.word1);

        return (double) (bigramCount - 0.5) / word1Count;
    }

    public double KatzBackOff(bigramWord pair) {
        double sum1 = 0;
        for (int i = 0; i < trainWords.size(); i++) {
            sum1 = sum1 + absoluteDiscounting(new bigramWord(trainWords.get(i), pair.word2));
        }
        double a_y = 1 - sum1;

        double sum2 = 0;
        for (int i = 0; i < trainWords.size(); i++) {
            sum2 = sum2 + countWord(trainWords.get(i));
        }
        double b_z = countWord(pair.word1) / sum2;

        if (countBigram(pair) > 0) {
            return absoluteDiscounting(pair);
        } else {
            return a_y * b_z;
        }
    }

    public double JointProbability(bigramWord pair) {
        int count = countBigram(pair);
        return (double) count / bigramWordList.size();
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        try {
            BigramQueryApplication bqa = new BigramQueryApplication();
            Scanner input = new Scanner(System.in);
            System.out.println("Enter the training data file. Append the required extension to the files.");
            String training = input.nextLine();
            System.out.println("Enter the test data file. Append the required extension to the files.");
            String test = input.nextLine();
            LanguageModelBuilder lmb = new LanguageModelBuilder(training, test);
            lmb.getBigramList();
            ArrayList<bigramWord> bigramsList = lmb.getBigramWordList();
            lmb.setDataBigrams();
            FileController.saveJSONFile(lmb.getBigrams());
            lmb.files.TopBigrams(lmb.getBigrams());
            int choice;
            int k = 1;
            do {
                LanguageModelEvaluator pp = new LanguageModelEvaluator(lmb.files, lmb.getBigrams(), lmb);
                choice = bqa.BQA(lmb.getBigrams(), k);
                pp.getPerplexity(choice);
                k++;
            } while (choice != 4); 
        
        } catch (FileNotFoundException e) {
            System.out.println("THE FILES ENTERED IS NOT FOUND. Please check and run again.");
        } catch (IOException f) {
            System.out.println("THE APPLICATION HAS CASHED. Please load again.");
        } catch (NumberFormatException g) {
            System.out.println("CHECK THE INPUT FORMAT. Please load again.");
        }
    }
}
